﻿namespace ScanProt2
{
    using System;
    using System.IO;
    using System.IO.Ports;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using Microsoft.Kinect;
    using Microsoft.Kinect.Toolkit.Fusion;
    using Microsoft.KinectFusionHelper; //Classe adaptada do toolkit para gerar malha sem cor

    public partial class MainWindow : Window, IDisposable
    {
        private SerialPort porta;
        private bool baseStatus;
        private int angulo;
        private const DepthImageFormat resolucaoImg = DepthImageFormat.Resolution640x480Fps30;
        private KinectSensor sensor;
        private Reconstruction volume;
        private int comprimentoFrame;
        private bool processandoFrame;
        private const ReconstructionProcessor ProcessorType = ReconstructionProcessor.Amp;
        private bool disposed;
        private bool moverVolumeEmZ = true;
        private DepthImagePixel[] tempPixelDados;
        private WriteableBitmap corBitmap;
        private int[] tempCorPixel;
        private FusionFloatImageFrame tempDepthFloat;
        private FusionPointCloudImageFrame tempPointCloud;
        private FusionColorImageFrame imgTonsCinza;
        private Matrix4 visaoMundoEmSisCoor;
        private Matrix4 ultimaVisaoMundo;

        public MainWindow()
        {
            this.InitializeComponent();
        }
                
        ~MainWindow()
        {
            this.Dispose(false);
            
        }

        /// <summary>
        /// Métodos para descarte de memória - Garbage Collector
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (null != this.tempDepthFloat)
                {
                    this.tempDepthFloat.Dispose();
                }

                if (null != this.tempPointCloud)
                {
                    this.tempPointCloud.Dispose();
                }

                if (null != this.imgTonsCinza)
                {
                    this.imgTonsCinza.Dispose();
                }

                if (null != this.volume)
                {
                    this.volume.Dispose();
                }

                this.disposed = true;
            }
        }

        /// <summary>
        /// Método principal da aplicação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScannerInicio(object sender, RoutedEventArgs e)
        {
            buscarPortas();
            // verifica compatibilidade
            try
            {
                string pcInfo = string.Empty;
                string pcIntancia = string.Empty;
                int pcMemoria = 0;

                FusionDepthProcessor.GetDeviceInfo(
                    ProcessorType, -1, out pcInfo, out pcIntancia, out pcMemoria);
            }
            catch (IndexOutOfRangeException)
            {
                this.infoKinect.Text = "Dispositivo inconpatível, DirectX11 necessario.";
                return;
            }
            catch (DllNotFoundException)
            {
                this.infoKinect.Text = "Dll indisponível, verifique a instalação dos componentes.";
                return;
            }
            catch (InvalidOperationException ex)
            {
                this.infoKinect.Text = ex.Message;
                return;
            }

            //Busca sensores disponíveis
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }
            if (null == this.sensor)
            {
                this.infoKinect.Text = "Kinect não localizado!";
                return;
            }

            // Inicia o fluxo de dados de profundidade
            this.sensor.DepthStream.Enable(resolucaoImg);

            // Tamanho do fluxo de dados de profunidade
            this.comprimentoFrame = this.sensor.DepthStream.FramePixelDataLength;

            //Alocação de memória
            this.tempPixelDados = new DepthImagePixel[this.comprimentoFrame];
            this.tempCorPixel = new int[this.comprimentoFrame];
            this.corBitmap = new WriteableBitmap(
                (int)new Size(640, 480).Width,
                (int)new Size(640, 480).Height,
                96.0,
                96.0,
                PixelFormats.Bgr32,
                null); 
            
            this.Image.Source = this.corBitmap;
            this.sensor.DepthFrameReady += this.SensorDepthFrameReady; 

            var volParam = new ReconstructionParameters(256,128,128,128);

            this.tempDepthFloat = new FusionFloatImageFrame((int)new Size(640, 480).Width, 
                (int)new Size(640, 480).Height);
            this.tempPointCloud = new FusionPointCloudImageFrame((int)new Size(640, 480).Width, 
                (int)new Size(640, 480).Height);
            this.imgTonsCinza = new FusionColorImageFrame((int)new Size(640, 480).Width, 
                (int)new Size(640, 480).Height);
            
            this.visaoMundoEmSisCoor = Matrix4.Identity; 

            try
            {
                this.volume = Reconstruction.FusionCreateReconstruction(volParam, ProcessorType,
                    -1, this.visaoMundoEmSisCoor);
                
                this.ultimaVisaoMundo = this.volume.GetCurrentWorldToVolumeTransform();
                
                if (this.moverVolumeEmZ)
                {
                    this.ReiniciarRecons();
                }
            }
            catch (InvalidOperationException ex)
            {
                this.infoKinect.Text = ex.Message;
                return;
            }
            catch (DllNotFoundException)
            {
                this.infoKinect.Text = "Dll indisponível, verifique a instalação dos componentes.";
                return;
            }
                        
            // Iniciar sensor
            try
            {
                this.sensor.Start();
                this.angulo = 0;
                this.sensor.ElevationAngle = 0;
            }
            catch (IOException ex)
            {
                this.sensor = null;
                this.infoKinect.Text = ex.Message;
                return;
            }
            catch (InvalidOperationException ex)
            {
                this.sensor = null;
                this.infoKinect.Text = ex.Message;
                return;
            }            
        }

        /// <summary>
        /// Finaliza conexão serial e comunicação com o sensor ao encerrar o programa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScannerFim(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (porta != null)
            {
                rotacionarBase("1");
                porta.Close();
                porta = null;
            }

            if (null != this.sensor)
            {
                this.sensor.Stop();
            }            
        }
        
        /// <summary>
        /// Manipulador de eventos para busca de novos frames
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            //Recebe próximo frame disponível
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                // Verifica se o ultimo frame já foi processado
                if (depthFrame != null && !this.processandoFrame)
                {
                    // Copia frame para armazenamento local
                    depthFrame.CopyDepthImagePixelDataTo(this.tempPixelDados);

                    // Marca que o frame sera processado
                    this.processandoFrame = true;
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)(() => this.ProcessarFrame()));                   
                }                                
            }
        }

        /// <summary>
        /// Realiza processamento do frame atual
        /// </summary>
        private void ProcessarFrame()
        {
            this.infoKinect.Text = "Reconstruindo...";
            try
            {
                this.volume.DepthToDepthFloatFrame(
                    this.tempPixelDados,
                    this.tempDepthFloat,
                    0.35f,
                    1.10f,
                    false);

                bool statusAlinhamento = this.volume.ProcessFrame(
                    this.tempDepthFloat,
                    FusionDepthProcessor.DefaultAlignIterationCount,
                    FusionDepthProcessor.DefaultIntegrationWeight,
                    this.volume.GetCurrentWorldToCameraTransform());

                
                if (!statusAlinhamento)
                {
                    this.infoKinect.Text = "Erro de alinhamento.";                    
                }
                else
                {  
                    Matrix4 matrizAtual = this.volume.GetCurrentWorldToCameraTransform();
                    
                    this.visaoMundoEmSisCoor = matrizAtual;                   
                }
                
                this.volume.CalculatePointCloud(this.tempPointCloud, this.visaoMundoEmSisCoor);
                
                FusionDepthProcessor.ShadePointCloud(
                    this.tempPointCloud,
                    this.visaoMundoEmSisCoor,
                    this.imgTonsCinza,
                    null);
                
                this.imgTonsCinza.CopyPixelDataTo(this.tempCorPixel);

                // Escrever os dados no bitmap
                this.corBitmap.WritePixels(
                    new Int32Rect(0, 0, this.corBitmap.PixelWidth, this.corBitmap.PixelHeight),
                    this.tempCorPixel,
                    this.corBitmap.PixelWidth * sizeof(int),
                    0);
                
            }
            catch (InvalidOperationException ex)
            {
              this.infoKinect.Text = ex.Message;
            }
            finally
            {
                this.processandoFrame = false;                
            }
        }
        
        /// <summary>
        /// Reiniciar a reconstrução 
        /// </summary>
        private void ReiniciarRecons()
        {
            this.visaoMundoEmSisCoor = Matrix4.Identity;

            if (null != this.volume)
            {
                if (this.moverVolumeEmZ)
                {
                    Matrix4 visaoTemp = this.ultimaVisaoMundo;
                    // Move o volume de reconstrução para menor distancia no eixo Z
                    visaoTemp.M43 -= 0.35f * 256; //voxels por metro
                    this.volume.ResetReconstruction(this.visaoMundoEmSisCoor, visaoTemp);
                }
                else
                {
                    this.volume.ResetReconstruction(this.visaoMundoEmSisCoor);
                }
            }            
        }

        
        private void btResetar_Click(object sender, RoutedEventArgs e)
        {
            if (null == this.sensor)
            {
                this.infoKinect.Text = "Kinect não disponível, conecte e reinicie a aplicação.";
                return;
            }          
              
            this.ReiniciarRecons();
            this.infoKinect.Text = "Reconstrução reiniciada.";
        }

       
        private void btSalvar_Click(object sender, RoutedEventArgs e)
        {
            if(baseStatus == true)
            {
                rotacionarBase("1");
            }

            try
            {
                this.infoKinect.Text = "Criando e salvando malha de reconstrução, por favor aguarde...";

                Mesh mesh = null;
                              
                if (null == this.volume)
                {
                    this.infoKinect.Text = "Volume de reconstrução indisponível.";
                    return;
                }

                mesh = volume.CalculateMesh(1);
                
                if (null == mesh)
                {
                    this.infoKinect.Text = "Erro ao salvar, reinicie a reconstrução.";
                    return;
                }

                Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();                               
                dialog.FileName = "Scanner.stl";
                dialog.Filter = "STL Mesh Files|*.stl|All Files|*.*";
                               
                if (true == dialog.ShowDialog())
                {                    
                    using (BinaryWriter writer = new BinaryWriter(dialog.OpenFile()))
                    {
                        Helper.SaveBinaryStlMesh(mesh, writer, true);
                    }                    
                    this.infoKinect.Text = "Reconstrução salva com sucesso!";
                }
                else
                {
                    this.infoKinect.Text = "Solicitação cancelada.";
                }
            }
            catch (ArgumentException)
            {
                this.infoKinect.Text = "Erro ao salvar, reinicie a reconstrução.";
            }
            catch (InvalidOperationException)
            {
                this.infoKinect.Text = "Erro ao salvar, reinicie a reconstrução.";
            }
            catch (IOException)
            {
                this.infoKinect.Text = "Erro ao salvar, reinicie a reconstrução.";
            }
            catch (OutOfMemoryException)
            {
                this.infoKinect.Text = "Erro ao salvar, memória insuficiente.";
            }            
        }

        /// <summary>
        /// Realiza busca das portas seriais disponíveis
        /// </summary>
        public void buscarPortas()
        {
            //Buscar Porta Serial Disponível e Controle Lógico de botões
            foreach (string s in SerialPort.GetPortNames())
            {
                comboBox.Items.Add(s);
            }
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
                btConectar.IsEnabled = true;
                this.infoBase.Text = "Base Disponível! Inicie a conexão.";
            }
            else
            {
                this.infoBase.Text = "Base indisponível. Conecte na porta USB.";
                btConectar.IsEnabled = false;                
            }
        }

        /// <summary>
        /// Realiza o envio de comandos para o arduino(base)
        /// </summary>
        /// <param name="comando">Código da função desejada</param>
        private void rotacionarBase(string comando)
        {
            if (porta != null)
            {
                if (porta.IsOpen == true)
                {
                    if (baseStatus)
                    {
                        porta.Write(comando);                                              
                    }
                    else
                    {
                        porta.Write(comando);
                    }
                }
            }
        }

        private void btAtualizar_Click(object sender, RoutedEventArgs e)
        {
            comboBox.Items.Clear();
            foreach (string s in SerialPort.GetPortNames())
            {
                comboBox.Items.Add(s);
            }
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedIndex = 0;
                btConectar.IsEnabled = true;
                this.infoBase.Text = "Base Localizada!";
            }
            else
            {
                this.infoBase.Text = "Base indisponível. Conecte na porta USB.";
                btConectar.IsEnabled = false;
            }
        }

        private void btConectar_Click(object sender, RoutedEventArgs e)
        {
            if (porta == null)
            {
                try
                {
                    porta = new SerialPort(comboBox.Items[comboBox.SelectedIndex].ToString());
                    porta.BaudRate = 9600;
                    porta.Open();               
                    btConectar.Content = "Desconectar";
                    comboBox.IsEnabled = false;
                    btAtualizar.IsEnabled = false;
                    this.infoBase.Text = "Comunicação Iniciada! Mantenha USB conectada.";
                    baseStatus = true;
                    rotacionarBase("0");
                }
                catch
                {
                    return;
                }
            }
            else
            {
                try
                {
                    baseStatus = false;
                    rotacionarBase("1");
                    porta.Close();
                    porta = null;
                    btConectar.Content = "Conectar";
                    comboBox.IsEnabled = true;
                    btAtualizar.IsEnabled = true;
                    this.infoBase.Text = "Comunicação Encerrada!";                    
                }
                catch
                {
                    return;
                }
            }
        }

        private void btSubir_Click(object sender, RoutedEventArgs e)
        {
            if(this.sensor != null)
            {
                this.angulo += 5;
                if (angulo <= 27)
                {
                    this.sensor.ElevationAngle = angulo;
                    infoKinect.Text = "Modificações constantes podem danificar o sensor.";
                }
                else
                {
                    this.angulo = this.sensor.MaxElevationAngle;
                    this.sensor.ElevationAngle = angulo;
                    infoKinect.Text = "Inclinação Máxima Atingida (" + angulo + "º).";
                }
            }
            else
            {
                infoKinect.Text = "Sensor não conectado";
            }            
        }

        private void btCentro_Click(object sender, RoutedEventArgs e)
        {
            if(this.sensor != null)
            {
                this.angulo = 0;
                this.sensor.ElevationAngle = angulo;
                infoKinect.Text = "Inclinação de 0º definida.";
            }
            else
            {
                infoKinect.Text = "Sensor não conectado";
            }
        }

        private void btDescer_Click(object sender, RoutedEventArgs e)
        {
            if(this.sensor != null)
            {
                this.angulo -= 5;
                if (angulo >= -27)
                {
                   this.sensor.ElevationAngle = angulo;
                    infoKinect.Text = "Modificações constantes podem danificar o sensor.";
                }
                else
                {
                    this.angulo = this.sensor.MinElevationAngle;
                    this.sensor.ElevationAngle = angulo;
                    infoKinect.Text = "Inclinação Mínima Atingida (" + angulo + "º).";
                }
            }
            else
            {
                infoKinect.Text = "Sensor não conectado";
            }
        }
    }
}